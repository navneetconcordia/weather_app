import React from "react";
import CurrentLocation from "./currentLocation";
import "./App.css";

function App() {
  return (
    <div className="container">
      <CurrentLocation />
      <div className="footer-info">
        Designed by <a>Navneet Singh Chhabra</a>
      </div>
    </div>
  );
}

export default App;